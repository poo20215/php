<?php
/**
 * Devuelve un div con el texto pasado
 * @param string $mensaje  Texto a colocar en el div
 * @return string Etiqueta creadad con el texto
 */
    function crearEtiqueta($mensaje){
        return "<div>{$mensaje}</div>";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $texto="Ejemplo de clase";
        $salida= crearEtiqueta($texto);
        echo $salida;
        
        echo crearEtiqueta("Probando mi funcion");
        
        ?>
    </body>
</html>
