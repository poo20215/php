<?php
    function mostrar($nombre,$edad){
        echo "Nombre: {$nombre}<br>";
        echo "Edad: {$edad}<br>";
    }
    
    function mostrar1($valores){
        echo "Nombre: {$valores["nombre"]}<br>";
        echo "Edad: {$valores["edad"]}<br>";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $nombre="Ramon";
            $edad=48;
            mostrar($nombre,$edad);
            
            $datos=[
                "nombre" => "ramon",
                "edad" => "25"
            ];
            mostrar1($datos);
        ?>
    </body>
</html>
