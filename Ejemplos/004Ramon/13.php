<?php
    /*
     * Ejemplo de etiqueta a crear
     * <div style="background-color: coral;
     *              width:400px;text-align: center;margin:10px auto">
     * </div>
     */

    function colocarEstilos($estilos){
        $salida='style="';
        
        foreach ($estilos as $indice=>$valor){
            $salida="{$salida}{$indice}:{$valor};";
        }
        
        $salida=$salida . '"';
        
        return $salida;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $aspecto=[
            "width" => "200",
            "color" => "white",
            "background-color" => "black",
            "margin" => "10px auto",
            "text-align" => "center",
            "line-height" => "80px",
        ];
        
        ?>
        <div <?= colocarEstilos($aspecto) ?> > Probando </div>
    </body>
</html>
