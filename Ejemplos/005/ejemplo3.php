<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        function operacion($numero1,$numero2,$operacion)
        {
            if($operacion=="suma")
            {
                return $numero1+$numero2;
            }elseif($operacion=="producto")
            {
                return $numero1*$numero2;           
            }else
            {
                echo "Operación no implementada";           
            }
        }
        
        echo $salida=operacion(5,10,"suma");
        echo "<br>";
        echo $salida=operacion(5,10,"producto");
        echo "<br>";
        echo $salida=operacion(5,10,"resta");
        
        ?>
    </body>
</html>
