<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function operacion($a,$b,$c)
        {
            $c=$a+$b;
        }
        
        function operacionConRetorno($a,$b)
        {
            $c=$a+$b;
            return $c;
        }
        
        function operacionReferencia($a,$b,&$c)
        {
           $c=$a+$b; 
        }
        
        $numero1=20;
        $numero2=10;
        $resultado=0;
        operacion($numero1,$numero2,$resultado);
        $resultado2=operacionConRetorno($numero1,$numero2);
        operacionReferencia($numero1, $numero2, $resultado3);
        echo $resultado;
        echo "<br>";
        echo $resultado2;
        echo "<br>";
        echo $resultado3;
        
        ?>
    </body>
</html>
