<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
Crear un array denominado alumnos con los siguientes campos
id,nombre,apellidos,nota
Introducimos los siguientes registros
1,Ana,Vazquez,9
2,Jose,Lopez,6
3,Luisa,Marcano, 9
Quiero que mostreis:
 - todos los registros
 - calcular la nota media

        </pre>
        <?php
        $suma_notas=0;
        $alumnos=[["id"=>1,"nombre"=>"Ana","apellido"=>"Vazquez","nota"=>9],
                  ["id"=>2,"nombre"=>"Jose","apellido"=>"Lopez","nota"=>6],
                  ["id"=>3,"nombre"=>"Luisa","apellido"=>"Marcano","nota"=>9]];
        
        foreach($alumnos as $registros)
        {
            foreach($registros as $indice=>$valor)
            {
                echo $indice ."=>" . $valor . "<br>";
            }
            $suma_notas=$suma_notas+$registros["nota"];
        }
        $num=count($alumnos);
        echo "Nota media= " . ($suma_notas/$num);
        ?>
    </body>
</html>
