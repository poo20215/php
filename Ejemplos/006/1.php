<?php 
// creando una clase
    class Persona{
        //propiedades publicas
        public $nombre=null;
        public $apellido=null;
        public $edad;
        
        //propiedad privada
        private $tratamiento="Sr/a";
        
            
        // metodos publicos
        
        // getter
        public function getNombre(){
            return $this->tratamiento . " " . $this->nombre;
        }
        
        // setter
        public function setNombre($nombre){
            $this->nombre = strtoupper($nombre);
        }
        
        public function nombreCompleto(){
            return $this->getNombre() . " " . $this->apellido;
        }
        
        public function datos(){
            echo "<br>nombre : " . $this->nombre; 
            echo "<br>Apellido: " . $this->apellido;
            echo "<br>Edad: " . $this->edad;
            echo "<br> Iniciales: " . $this->calcularIniciales();
        }

        //metodos privados
        private function calcularIniciales(){
            return $this->nombre[0] .  ". " . $this->apellido[0] . ". ";
            //return substr($this->nombre,0,1) .  ". " . substr($this->apellido,0,1) . ". ";
        }
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
       //crear un objeto
        //istanciar 
        $persona1=new Persona();
        var_dump($persona1);
        //introducir datos en persona1
        $persona1->nombre="Susana";
        $persona1->apellido="López";
        
        //mostrando los datos
        $persona1->datos();
        
        //mostrando el nombre a traves del setter
        $persona1->setNombre("Susana");
        
        //mostrando el nombre
        echo "<br>" . $persona1->getNombre();
    
        ?>
    </body>
</html>
