<?php
spl_autoload_register(function ($nombre_clase) {
    include "ejemplo7/$nombre_clase" . '.php';
    });
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $animal=new Animales();
        $animal->setNombre("Popy");
        $animal->setRaza("Pequines");
        $animal->setColor("Marrón");
        $animal->setFechaNacimiento("2021/09/07");
        echo $animal->datos();
        $ciudad=new Ciudad();
        $ciudad->setNombre("Santander");
        $ciudad->setProvincia("Cantabria");
        echo $ciudad->obtenerIniciales(5);
        echo "<br>";
        $ciudad2=new Ciudad();
        $ciudad2->setNombre("Oviedo");
        $ciudad2->setProvincia("Asturias");
        echo $ciudad2->obtenerIniciales(3);
        ?>
    </body>
</html>
