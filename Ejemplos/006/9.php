<?php
spl_autoload_register(function ($nombre_clase) {
    include "$nombre_clase" . '.php';
    });
    
    use ejemplo9\Circulo;
    
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $circulo=new Circulo(15,30,50);
        echo "Area= " . $circulo->Area() . "<br>";
        echo "Perimetro= " . $circulo->Perimetro() . "<br>";
        $circulo->pintar();
        ?>
    </body>
</html>
