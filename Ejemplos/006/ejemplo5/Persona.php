<?php


/**
 * Description of Persona
 *
 * @author Profesor Ramon
 */
namespace ejemplo5;

class Persona {
    public $nombre;
    
    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre): void {
        $this->nombre = $nombre;
    }
}
