<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Animales
 *
 * @author Alpe
 */
namespace ejemplo7;

class Animales 
{
    public $nombre;
    private $raza;
    protected $color;
    private $fecha_nacimiento;
    
    public function getNombre() 
    {
        return $this->nombre;
    }

    public function getRaza() 
    {
        return $this->raza;
    }

    public function getColor() 
    {
        return $this->color;
    }
    
    public function getFechaNacimiento() 
    {
        return $this->fecha_nacimiento;
    }

    public function setNombre($nombre): void 
    {
        $this->nombre = $nombre;
    }

    public function setRaza($raza): void 
    {
        $this->raza = $raza;
    }

    public function setColor($color): void 
    {
        $this->color = $color;
    }
    
    public function setFechaNacimiento($fecha_nacimiento): void 
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }
    
    public function datos() 
    {
        return "<ul><li>$this->nombre</li><li>$this->raza</li><li>$this->color</li><li>$this->fecha_nacimiento</li></ul>";
    }
}
