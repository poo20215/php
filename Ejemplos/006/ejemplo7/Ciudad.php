<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ciudad
 *
 * @author Alpe
 */
namespace ejemplo7;

class Ciudad 
{
    public $nombre;
    private $provincia;
    
    public function getNombre() 
    {
        return $this->nombre;
    }
    
    public function getProvincia() 
    {
        return $this->provincia;
    }
    
    public function setNombre($nombre): void 
    {
        $this->nombre = $nombre;
    }
    
    public function setProvincia($provincia): void 
    {
        $this->provincia = $provincia;
    }

    public function obtenerIniciales($numero)
    {
        return substr($this->nombre,0,$numero) . " " . substr($this->provincia,0,$numero);
    }
}
