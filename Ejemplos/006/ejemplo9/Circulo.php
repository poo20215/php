<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Circulo
 *
 * @author Alpe
 */
namespace ejemplo9;

class Circulo {
    private $radio;
    private $centrox;
    private $centroy;
    
    public function __construct($radio=50, $centrox=25, $centroy=25) 
    {
        $this->radio = $radio;
        $this->centrox = $centrox;
        $this->centroy = $centroy;
    }
    
    public function getRadio() 
    {
        return $this->radio;
    }

    public function getCentrox() 
    {
        return $this->centrox;
    }

    public function getCentroy() 
    {
        return $this->centroy;
    }

    public function setRadio($radio): void 
    {
        $this->radio = $radio;
    }

    public function setCentrox($centrox): void 
    {
        $this->centrox = $centrox;
    }

    public function setCentroy($centroy): void 
    {
        $this->centroy = $centroy;
    }
    
    public function Area()
    {
        return pi()* $this->radio**2; //** para elevar al valor insertado despues(2 en este caso)
    }
    
    public function Perimetro()
    {
        return 2*pi()*$this->radio;
    }
    
    public function pintar()
    {
        echo "<svg height='100' width='100'>";
        echo "<circle cx='$this->centrox' cy='$this->centroy' r='$this->radio' stroke='black' stroke-width='3' fill='red' />";
        echo "</svg>";
    }
    
}
