<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $radio=$_POST["radio"];
        
        $longitud=2*pi()*$radio;
        echo "Longitud de la circunferencia = " . $longitud . "<br>";
        $area=pow($radio,2)*pi();
        echo "Área del círculo = " . $area . "<br>";
        $volumen=(4/3)*pi()*pow($radio,3);
        echo "Volumen de la esfera = " . $volumen;
        ?>
    </body>
</html>
