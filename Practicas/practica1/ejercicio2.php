<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio2</title>
    </head>
    <body>
        <h1>Ejercicio 2 de la practica 1</h1>
        <table width="100%" border="1">
            <tr>
                <td>
                <?php
                //Podemos utilizar o comillas simples o comillas dobles para el texto
                echo "Este texto esta escrito utilizando la funcion echo de PHP";
                ?>
                </td>
                <td>Este texto esta escrito en HTML</td>
            </tr>
            <tr>
                <td>
                    <?php
                    print 'Este texto esta escrito desde PHP con la funcion print';
                    ?>
                </td>
                <td>
                    <?= "Centro de formacion Alpe" ?>
                </td>
            </tr>
        </table>
      
    </body>
</html>
