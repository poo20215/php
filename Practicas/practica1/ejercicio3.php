<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 3</title>
    </head>
    <body>
        <h2> Metodo 1</h2>
        <?php
        //mezclamos html y php
        echo "<p> Hola mundo </p>";
        ?>
        <h2> Metodo 2</h2>
        <p>
            <?php
            //solo coloco php
            echo "hola mundo";
            ?>
        </p>
        
        <h2> Metodo 3</h2>
        <?php
        echo "<p>";
        ?>
        hola mundo
        <?php
        echo "</p>";
        ?>
        
        <h2>Podemos utilizar el modo contraido del echo</h2>
        <p>
            <?= "hola mundo" ?>
        </p>
    </body>
</html>
