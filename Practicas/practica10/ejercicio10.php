<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo __FILE__;  //muestra la ubicacion de este archivo
        echo "<br>";
        echo __LINE__;  //muestra la linea en la que nos encontramos
        echo "<br>";
        echo PHP_VERSION; //constante definida por el nucleo PHP
        echo "<br>";
        echo PHP_OS; //constante definida por el nucleo PHP
        echo "<br>";
        echo __DIR__; //muestra el directorio de este archivo
        echo "<br>";
        ?>
    </body>
</html>
