<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Funcion que genera un array con numeros aleatorios.
         * @param int $min
         * @param int $max
         * @param int $num_valores
         * @return array
         */
        function vector($min,$max,$num_valores)
        {
            for($x=0;$x<$num_valores;$x++)
            {
                $vector[]=rand($min,$max);
            }
            return $vector;
        }
        
        $salida=vector(1,10,8);
        var_dump($salida);
        ?>
    </body>
</html>
