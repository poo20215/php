<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Funcion para crear un color aleatorio en RGB, a la funcion le pasamos el numero de colores a generar.
         * @param int $num
         * @return array
         */
        function colores($num)
        {
            for($x=0;$x<$num;$x++)
            {
                $vector[$x]="#";
                for($y=0;$y<7;$y++)
                {
                     $vector[$x].=dechex(mt_rand(0,15));
                }
            }
            return $vector;
        }
        
        $salida= colores(9);
        var_dump($salida);
        ?>
    </body>
</html>
