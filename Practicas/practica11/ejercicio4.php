<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Funcion que genera colores aleatorios en RGB, a la funcion le pasamos el numero de colores a generar y si queremos que estos colores empiecen por # o no. 
         * @param int $num
         * @param bool $bool
         * @return array
         */
        function colores($num,$bool=true)
        {
            for($x=0;$x<$num;$x++)
            {
                if($bool==true)
                {
                    $vector[$x]="#";
                    $limite=7;
                }else
                {
                    $vector[$x]="";
                    $limite=6;
                }
                for($y=0;$y<$limite;$y++)
                {
                     $vector[$x].=dechex(mt_rand(0,15));
                }
            }
            return $vector;
        }
        
        $salida= colores(9,false);
        var_dump($salida);
        ?>
    </body>
</html>