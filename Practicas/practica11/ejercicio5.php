<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function elementosRepetidos($array, $devolverTodos=false)
        {
            $repeated=array();
            
            foreach((array) $array as $value)
            {
                $inArray=false;
                
                foreach($repeated as $i => $rItem)
                {
                    if($rItem['value']===$value)
                    {
                        $inArray=true;
                        ++$repeated[$i]['count'];
                    } 
                }
                
                if($inArray===false)
                {
                    //$i=count($repeated);
                    $repeated[$i]=array();
                    $repeated[$i]['value']=$value;
                    $repeated[$i]['count']=1;
                }
            }
            
            if(!$devolverTodos)
            {
                foreach($repeated as $i => $rItem)
                {
                    if($rItem['count']===1)
                    {
                        unset($repeated[$i]);
                    }
                }
            }
            
            sort($repeated);
            return $repeated;
        }
        $vector=[5,7];
        $salida=elementosRepetidos($vector);
        ?>
    </body>
</html>
