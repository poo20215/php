<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function mostrar()
        {
            static $c=0;
            foreach(func_get_args() as $valor)
            {
                echo "<br/>$c-$valor<br/>";
                $c++;
            }
        }
        
        /*
         * Analizamos las funciones date() y time()
         */
        
        $fechaActual=time();
        
        mostrar("Raro, raro:",$fechaActual);
        mostrar(date("d/m/Y,$fechaActual"));
        
        mostrar(date("d/m/y"));
        mostrar(date("j"),date("d"),date("D"));
        mostrar(date("N"),date("F"),date("m"));
        mostrar(date("Y"),date("y"));
        
        ?>
    </body>
</html>
