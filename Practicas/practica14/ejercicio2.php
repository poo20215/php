<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function mostrar()
        {
            foreach(func_get_args() as $valor)
            {
                echo "<br/>$valor<br/>";
            }
        }
        
        /*
         * Analizamos las funciones mktime() y checkdate()
         */
        
        $dia=23;
        $mes=1;
        $year=2004;
        $fecha=mktime(0,0,0,$mes,$dia,$year);
        mostrar(date("d/m/y",$fecha));
        
        $dia=35;
        $mes=1;
        $year=2004;
        if(checkdate($mes,$dia,$year))
        {
            $fecha=mktime(0,0,0,$mes,$dia,$year);
        }else
        {
            $fecha=mktime(0,0,0,12,1,2000);
        }
        
        mostrar(date("d/m/y",$fecha));
        ?>
    </body>
</html>
