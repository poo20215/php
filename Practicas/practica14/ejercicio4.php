<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function mostrar()
        {
            foreach(func_get_args() as $valor)
            {
                echo "<br/>$valor<br/>";
            }
        }
        
        setlocale(LC_ALL, 'spanish');
        mostrar(strftime("%A%B"));
        mostrar(strftime("%A",mktime(0,0,0,1,1,1901)));
        ?>
    </body>
</html>
