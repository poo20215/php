<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function mostrar()
        {
            foreach(func_get_args() as $valor)
            {
                echo "<br/>$valor<br/>";
            }
        }
        
        /*
         * Vamos a analizar la funcion strtotime()
         */
        
        $fecha= "2000/12/25";
        
        mostrar(date("d/m/y", strtotime($fecha)));
        mostrar(date("d/m/y", strtotime("now")));
        mostrar(date("d/m/y", strtotime("+1 day")));
        mostrar(date("d/m/y", strtotime("+1 day", strtotime($fecha))));
        mostrar(date("d/m/y", strtotime("previus Monday")));
        
        //La funcion strtotime() convierte una fecha en straing a una fecha Unix.
        
        $fecha= "10/12/2012";
        $fecha=explode("/", $fecha); //crea una array con los valores que estan separados por /
        $fecha=implode("/", array_reverse($fecha)); //crea un string con cada elemento del array $fecha separados por /
        mostrar(date("d/m/Y", strtotime($fecha)));
        
        
        ?>
    </body>
</html>
