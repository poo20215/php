<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="post">
                <label for="fecha_inicial">Introduce fecha inicial</label>
                <br>
                <input type="date" name="fecha_inicial" id="fecha_inicial" placeholder="dd/mm/aaaa" required="">
                <br>
                <label for="fecha_final">Introduce fecha fin</label>
                <br>
                <input type="date" name="fecha_final" id="fecha_final" placeholder="dd/mm/aaaa" required>
                <br>
		<input type="submit" value="Calcular" name="calcular">
        </form>
        <?php
        if(isset($_POST["calcular"]))
        {
            $fecha_inicial=$_POST["fecha_inicial"];
            $fecha_final=$_POST["fecha_final"];
            $fecha_inicial=strtotime($fecha_inicial);
            $fecha_final=strtotime($fecha_final);
            
            if($fecha_inicial<=$fecha_final)
            {
               $resta=$fecha_final-$fecha_inicial;
               $segundos_dia=24*60*60;
               $dias=$resta/$segundos_dia;
               echo "<br>" . "Diferencia en dias: " . $dias;
            }else
            {
                echo "<br>" . "La fecha final ha de ser mayor o igual que la fecha inicial";
            }
        }
        ?>  
    </body>
</html>
        