<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
	<style>
	div{
            color: red;
            border: 1px solid crimson;
            margin: 10px auto;
            min-height: 100px;
            width: 250px;
        }
	</style>
    <body>
        <form method="post">
                <label for="fecha_inicial">Introduce fecha inicial</label>
                <br>
                <input type="text" name="fecha_inicial" id="fecha_inicial" placeholder="dd/mm/aaaa" required="">
                <br>
                <label for="fecha_final">Introduce fecha fin</label>
                <br>
                <input type="text" name="fecha_final" id="fecha_final" placeholder="dd/mm/aaaa" required>
                <br>
		<input type="submit" value="Calcular" name="calcular">
        </form>
        <?php
        if(isset($_POST["calcular"]))
        {
            $fecha_inicial=$_POST["fecha_inicial"];
            $fecha_final=$_POST["fecha_final"];
            $ok_i=preg_match("/^\d{1,2}\/\d{1,2}\/\d{4}$/",$fecha_inicial);
            $ok_f=preg_match("/^\d{1,2}\/\d{1,2}\/\d{4}$/",$fecha_final);
            if($ok_i)
            {
                $array_inicial=explode("/",$fecha_inicial);
                $resultado_i=checkdate($array_inicial[1],$array_inicial[0],$array_inicial[2]);
                if($resultado_i)
                {
                    $fecha_inicial=implode("-",$array_inicial);
                    $fecha_inicial=strtotime($fecha_inicial);
                } else
                {
                      echo "<div>Formato fecha inicial incorrecto</div>";                  
                }
            }else
            {
                echo "<div>Formato fecha inicial incorrecto</div>";
                $resultado_i=false;
            }
			
            if($ok_f)
            {
                $array_final=explode("/",$fecha_final);
                $resultado_f=checkdate($array_final[1],$array_final[0],$array_final[2]);
                if($resultado_f)
                {
                    $fecha_final=implode("-",$array_final);
                    $fecha_final=strtotime($fecha_final);
                }else
                {
                    echo "<div>Formato fecha final incorrecto</div>";
                }
	    }else
            {
                echo "<div>Formato fecha final incorrecto</div>";
                $resultado_f=false;
            }
            
            if(($resultado_i) and ($resultado_f))
            {
                if($fecha_inicial<=$fecha_final)
                {
                    $resta=$fecha_final-$fecha_inicial;
                    $segundos_dia=24*60*60;
                    $dias=$resta/$segundos_dia;
                    echo "<br>";
                    echo $_POST["fecha_inicial"] . "-->" . $_POST["fecha_final"];
                    echo "<br>";
                    echo "Diferencia en dias: " . $dias;
                }else
                {
                    echo "<div>La fecha final ha de ser mayor o igual que la fecha inicial</div>";
                }
	    }
        }
        ?>  
    </body>
</html>
        