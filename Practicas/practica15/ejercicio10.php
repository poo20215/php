<?php
if($_REQUEST)
{
    $mal=false;
}else
{
    $mal=true;
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if($mal)
        {
            ?>
        <form name="f">
            <label for="ciudad">Seleccione la ciudad</label>
            <select multiple id="ciudad" name="ciudad[]">
                <optgroup label="Asia"> <!-- agrupa elementos de la lista y les pone un titulo. -->
                <option value="3">Delhi</option>
                <option value="4">Hong Kong</option>
                <option value="8">Mumbai</option>
                <option value="11">Tokyo</option>
                </optgroup>
                <optgroup label="Europe"> 
                <option value="1">Amsterdam</option>
                <option value="5">London</option>
                <option value="7">Moscow</option>
                </optgroup>
                <optgroup label="North America"> 
                <option value="6">Los Angeles</option>
                <option value="9">New York</option>
                </optgroup>
                <optgroup label="South America"> 
                <option value="2">Buenos Aires</option>
                <option value="10">Sao Paulo</option>
                </optgroup>
            </select>
            <input type="submit" value="Enviar" name="boton">
        </form>
        <?php
        }else
        {
            $ciudades=array(
                "","Amsterdam","Buenos Aires",
                "Delphi","Hong Kong","London",
                "Los Angeles","Moscu","Mumbai",
                "New York","Sao Paulo","Tokyo");
   
            echo "los elementos seleccionales son:";
            foreach($_REQUEST["ciudad"] as $value)
            {
                echo "<br>$ciudades[$value]";
            }
        }
   
        ?>
    </body>
</html>