<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <style>
        body{
            background-color: #ffcccc;
        }   
        h1{
	text-align:center;
        }
        table{
            margin:auto;
            border-collapse: collapse;  
        }
        .colum_i{
            width: 200px;
            border:solid 1px black;
            background-color:darkolivegreen;
            text-align:right;
        }
        .colum_d{
            background-color:darkgrey;
            border:solid 1px black;
        }
                      
    </style>
    
    <body>
        
        <?php
       
        
        if(isset($_POST["boton"]))
        {
            echo "Nombre Completo: " . $_POST["nombre"] . "<br>";
            echo "Dirección: " . $_POST["direccion"] . "<br>";
            echo "Correo Electrónico: " . $_POST["correo"] . "<br>";
            echo "Contraseña: " . $_POST["contrasena"] . "<br>";
            echo "Confirmar Contraseña: " . $_POST["confirmar_contrasena"] . "<br>";
            echo "Fecha de nacimiento: " . $_POST["dia"] . " " . $_POST["mes"] . " " . $_POST["ano"] . "<br>";
            if(isset($_POST["sexo"]))
            {
                echo "Sexo: " . $_POST["sexo"] . "<br>";
            } else 
            {
                echo "Sexo: No incluido" . "<br>";                
            }
            
            if(isset($_POST["t_interes"]))
            {
                $separado_por_comas=implode(", ",$_POST["t_interes"]);
                echo "Temas de interés: " . $separado_por_comas . "." . "<br>";
            } else 
            {
                echo "Temas de interés: No incluidos" . "<br>";                
            }
            
            if(isset($_POST["aficiones"]))
            {
                $separado_por_comas=implode(", ",$_POST["aficiones"]);
                echo "Aficiones: " . $separado_por_comas . ".";
            } else 
            {
                echo "Aficiones: No incluidas";                
            }
            
        }else
        {
        ?>
        
        <h1>Formulario de inscripción de usuarios</h1>
        
        <form method="post">
            <table>
                <tr>
                    <td class="colum_i"><br><label for="nombre">Nombre Completo&nbsp&nbsp&nbsp</label><br><br></td>
                    <td class="colum_d"><br>&nbsp&nbsp&nbsp<input type="text" name="nombre" id="nombre" placeholder="nombre"/><br><br></td>
                </tr>
                <tr>
                    <td class="colum_i"><br><label for="direccion">Dirección&nbsp&nbsp&nbsp</label><br><br></td>
                    <td class="colum_d"><br>&nbsp&nbsp&nbsp<input type="text" name="direccion" id="direccion" placeholder="d"/><br><br></td>
                </tr>
                <tr>
                    <td class="colum_i"><br><label for="correo">Correo&nbsp&nbsp&nbsp<br>Electrónico&nbsp&nbsp&nbsp</label><br><br></td>
                    <td class="colum_d"><br>&nbsp&nbsp&nbsp<input type="text" name="correo" id="correo" placeholder="c"/><br><br></td>
                </tr>
                <tr>
                    <td class="colum_i"><br><label for="contrasena">Contraseña&nbsp&nbsp&nbsp</label><br><br></td>
                    <td class="colum_d"><br>&nbsp&nbsp&nbsp<input type="password" name="contrasena" id="contrasena"/><br><br></td>
                </tr>
                <tr>
                    <td class="colum_i"><br><label for="confirmar_contrasena">Confirmar&nbsp&nbsp&nbsp<br>Contraseña&nbsp&nbsp&nbsp</label>
                    <br><br><br><label for="f_nacimiento">Fecha de&nbsp&nbsp&nbsp<br>nacimiento&nbsp&nbsp&nbsp</label><br><br></td>
                    <td class="colum_d">&nbsp&nbsp&nbsp<input type="password" name="confirmar_contrasena" id="confirmar_contrasena"/>
                    <br><br><br>&nbsp&nbsp&nbsp<select name="mes"> 
                                <option>Enero</option> 
                                <option>Febrero</option> 
                                <option>Marzo</option> 
                                <option>Abril</option> 
                                <option>Mayo</option> 
                                <option>Junio</option> 
                                <option>Julio</option> 
                                <option>Agosto</option> 
                                <option>Septiembre</option> 
                                <option>Octubre</option> 
                                <option>Noviembre</option> 
                                <option>Diciembre</option> 
                                </select> 
                                <select name="dia">
                                <option>01</option> 
                                <option>02</option> 
                                <option>03</option> 
                                <option>04</option> 
                                <option>05</option> 
                                <option>06</option> 
                                <option>07</option> 
                                <option>08</option> 
                                <option>09</option> 
                                <option>10</option> 
                                <option>11</option> 
                                <option>12</option> 
                                <option>13</option> 
                                <option>14</option> 
                                <option>15</option> 
                                <option>16</option> 
                                <option>17</option> 
                                <option>18</option> 
                                <option>19</option> 
                                <option>20</option> 
                                <option>21</option> 
                                <option>22</option> 
                                <option>23</option> 
                                <option>24</option> 
                                <option>25</option> 
                                <option>26</option> 
                                <option>27</option> 
                                <option>28</option> 
                                <option>29</option> 
                                <option>30</option> 
                                <option>31</option> 
                                </select>
                    <input style="width: 50px" type="text" name="ano"/><br>
                    </td>
                </tr>
                <tr>
                    <td class="colum_i"><br><label for="sexo">Sexo&nbsp&nbsp&nbsp</label>
                    <br><br><br><label for="f_nacimiento">Por favor elige&nbsp&nbsp&nbsp<br>los temas de tus&nbsp&nbsp&nbsp<br>intereses&nbsp&nbsp&nbsp</label><br><br></td>
                    <td class="colum_d"><br>&nbsp&nbsp&nbsp<input type="radio" name="sexo" value="Hombre" id="hombre"/><label for="hombre">Hombre</label>&nbsp&nbsp&nbsp<input type="radio" name="sexo" value="Mujer" id="mujer"/><label for="mujer">Mujer</label>
                        <br><br><br>&nbsp&nbsp&nbsp<input type="checkbox" name="t_interes[]" value="Ficción" id="ficcion"/><label for="ficcion">Ficcion</label>&nbsp&nbsp&nbsp<input type="checkbox" name="t_interes[]" value="Terror" id="terror"/><label for="terror">Terror</label>
                        <br>&nbsp&nbsp&nbsp<input type="checkbox" name="t_interes[]" value="Acción" id="accion"/><label for="accion">Acción</label>&nbsp&nbsp&nbsp<input type="checkbox" name="t_interes[]" value="Comedia" id="comedia"/><label for="comedia">Comedia</label>
                        <br>&nbsp&nbsp&nbsp<input type="checkbox" name="t_interes[]" value="Suspense" id="suspense"/><label for="suspense">Suspense</label><br><br></td>
                </tr>
                <tr>
                    <td class="colum_i"><br><label for="aficiones">Seleccina tus&nbsp&nbsp&nbsp<br>aficiones&nbsp&nbsp&nbsp</label>
                    <br><br><br><label>(Selecciona múltiples&nbsp&nbsp&nbsp<br>elementos pulsando la&nbsp&nbsp&nbsp<br>tecla Control y&nbsp&nbsp&nbsp<br>haciendo clic en cada&nbsp&nbsp&nbsp<br>uno, uno a uno)&nbsp&nbsp&nbsp</label><br><br></td>
                    <td class="colum_d"><br>&nbsp&nbsp&nbsp<select style="height: 125px" multiple name="aficiones[]">
                            <option value="Deportes al aire libre">Deportes al aire libre</option>
                            <option value="Deportes de aventuras">Deportes de aventuras</option>
                            <option value="Música Pop">Música Pop</option>
                            <option value="Música Rock">Música Rock</option>
                            <option value="Música alternativa">Música alternativa</option>
                            <option value="Fotografía">Fotografía</option>
                        </select>
                    <br><br></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center; background-color:darkgrey"><input style="width: 125px" type="submit" value="Enviar" name="boton"></td>
                </tr>
            </table>
        </form>
        <?php
        }
        ?>
        
    </body>
</html>
