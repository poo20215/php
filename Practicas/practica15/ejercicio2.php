<?php
if(empty($_REQUEST))
{
    $mal=true;
    $error="Introducir los datos en el formulario";
}elseif(empty($_REQUEST["numero"])) 
{
    $mal=true;
    $error="El número es obligatorio";
}elseif($_REQUEST["numero"]<0)
{
    $mal=true;
    $error="El número introducido debe ser mayor que 0";
}else
{
    $mal=false;
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        if(!$mal)
        {
            var_dump($_REQUEST);
        }else
        {
            echo $error;
        ?>
        <div>
            <form name="f">
                numero<input type="number" name="numero" value="" />
                <input type="submit" value="Enviar" name="boton" />
            </form>  
        </div>
        <?php
        }
        ?>
    </body>
</html>

<!-- Si no hemos enviado nada desde el formulario y por tanto el array $_REQUEST esta vacio 
nos pide introducir los datos en el formulario y nos muestra el formulario,
si hemos dado al boton de enviar pero no hemos introducido ningun número 
nos dice que el número es obligatorio y nos muestra el formulario,
si hemos enviado un número pero este es menor de 0 nos dice que el número ha de ser mayor que 0 y nos muestra el formulario,
y por último, si hemos enviado un número mayor o igual a 0 nos muestra a traves de un var_dump() la informacion del array $_REQUEST
con los datos generados mediante el formulario. -->