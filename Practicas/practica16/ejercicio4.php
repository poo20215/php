<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function calcular_area($a,$b,$c)
        {
            $p=($a+$b+$c)/2;
            $area=sqrt($p*($p-$a)*($p-$b)*($p-$c));
            return $area;
        }
        if(isset($_GET["boton"]))
        {
            $a=$_GET["lado_a"];
            $b=$_GET["lado_b"];
            $c=$_GET["lado_c"];     
            
            echo "Area= " . calcular_area($a,$b,$c);
        }else
        {
        ?>
        <form method="get">
            <label for="lado_a">Lado a: </label><input type="number" name="lado_a" id="lado_a" required><br>
            <label for="lado_b">Lado b: </label><input type="number" name="lado_b" id="lado_b" required><br>
            <label for="lado_c">Lado c: </label><input type="number" name="lado_c" id="lado_c" required><br>
            <input type="submit" name="boton" value="Calcular area">
        </form>
        <?php
        }
        ?>
    </body>
</html>
