<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        class Usuario
        {
            public $nombre="defecto";
            public function setNombre($nombre)
            {
                $this->nombre=$nombre;
            }
            public function getNombre()
            {
                return $this->nombre;
            }
        }
        
        $persona=new Usuario(); //creamos un nuevo objeto
        echo $persona->getNombre(); //llamamos al metodo y mostramos el valor devuelto
        $persona->setNombre("Silvia"); //cambiamos el valor del atributo nombre a traves de un setter
        var_dump($persona); //obtenemos información del objeto
        
        ?>
    </body>
</html>
