<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        class Usuario
        {
            public $nombre="defecto";
            private $edad;
            protected $telefono;
            
            public function getNombre() 
            {
                return $this->nombre;
            }
            public function getEdad() 
            {
                return $this->edad;
            }
            public function getTelefono() 
            {
                return $this->telefono;
            }
            public function setNombre($nombre): void 
            {
                $this->nombre = $nombre;
            }

            public function setEdad($edad): void 
            {
                $this->edad = $edad;
            }

            public function setTelefono($telefono): void 
            {
                $this->telefono = $telefono;
            }           
        }
        
        $persona=new Usuario(); //creamos un nuevo objeto de la clase Usuario
        echo $persona->nombre; //mostramos el valor de la propiedad nombre de la clase Usuario
        $persona->setEdad(51); //asignamos un valor a la propiedad edad mediante un setter
        $persona->setTelefono("232323"); //asignamos un valor a la propiedad telefono mediante un setter
        var_dump($persona); //obtenemos información del objeto persona
        $persona->nombre="Silvia"; //asignamos un valor a la propiedad nombre
        //$persona->edad=12;  nos da error ya que estamos intentando acceder a una propiedad privada 
        //$persona->telefono="202020";  nos da error ya que estamos intentando acceder a una propiedad protegida 
        var_dump($persona); //obtenemos información del objeto persona
        ?>
    </body>
</html>
