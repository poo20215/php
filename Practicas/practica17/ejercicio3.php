<?php
include "ejercicio3/Usuario.php";
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        $persona=new Usuario(); //creamos un nuevo objeto de la clase Usuario
        echo $persona->nombre; //mostramos el valor de la propiedad nombre de la clase Usuario
        $persona->setEdad(51); //asignamos un valor a la propiedad edad mediante un setter
        $persona->setTelefono("232323"); //asignamos un valor a la propiedad telefono mediante un setter
        $persona->setApellidos("Vazquez Rodrigez"); //asignamos un valor a la propiedad apellidos mediante un setter
        var_dump($persona); //obtenemos información del objeto persona
        $persona->nombre="Ramón"; //asignamos un valor a la propiedad nombre
        var_dump($persona); //obtenemos información del objeto persona
        $persona->setNombre("Ramón"); //asignamos un valor a la propiedad nombre mediante un setter
        var_dump($persona); //obtenemos información del objeto persona
        ?>
    </body>
</html>
