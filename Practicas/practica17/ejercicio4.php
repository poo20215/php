<?php
include "ejercicio4/Coche.php";
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        $coche=new Coche();  //instanciamos la clase Coche
        $coche->color="Rojo"; //asignamos un valor a la propiedad color
        $coche->marca="Honda"; //asignamos un valor a la propiedad marca
        $coche->numero_puertas=4; //asignamos un valor a la propiedad numero_puertas
        $coche->llenarTanque(10); //llamamos al metodo llenarTanque pasandole un valor requerido 
        $coche->acelerar(); //llamamos al metodo acelerar
        $coche->acelerar(); //llamamos al metodo acelerar
        $coche->acelerar(); //llamamos al metodo acelerar
        var_dump($coche); //obtenemos información del objeto coche
        
        ?>
    </body>
</html>
