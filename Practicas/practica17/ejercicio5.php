<?php
require './ejercicio5/Vehiculo.php';
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $ford=new Vehiculo("DHH2323","rojo",false); //creamos un nuevo objeto de la clase vehiculo, le pasamos las variables requeridas por el constructor.
        var_dump($ford); //muestra informacion del objeto ford
        $ford->apagar(); //llamada al metodo apagar mediante el objeto creado 
        
        $renault=new Vehiculo(); //nos da error al no pasarle las variables requeridas por el consrtuctor
        var_dump($renault);
        ?>
    </body>
</html>
