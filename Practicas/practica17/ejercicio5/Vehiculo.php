<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vehiculo
 *
 * @author diego
 */
class Vehiculo {
    
    public $matricula;
    private $color;
    protected $encendido;
    
    public function __construct($matricula, $color, $encendido) 
    {
        $this->matricula = $matricula;
        $this->color = $color;
        $this->encendido = $encendido;
    }

        public function encender() 
    {
        $this->encendido=true;
        echo "vehículo encendido <br />";
    }
    
    public function apagar() 
    {
        $this->encendido=false;
        echo "vehículo apagado <br />";
    }
        
}
   
