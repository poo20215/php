<?php
include_once './ejercicio6/Vehiculo.php';
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $ford=new Vehiculo("DHH2323","rojo",false); //creamos un objeto de la clase vehiculos y le pasamos el valor de las propiedades requeridas por el constructor
        $ford->mensaje(); //llamamos al metodo mensaje, nos permite llamarlo mediante el objeto a pesar de ser un metodo estático
        var_dump($ford); //muestra informacion del objeto
        echo $ford->ruedas(); //llamamos al metodo ruedas, nos permite llamarlo mediante el objeto a pesar de ser un metodo estático
        $ford::mensaje(); //llamamos al metodo estatico mensaje
        Vehiculo::mensaje(); //llamamos al metodo estatico mensaje mediante la clase
        Vehiculo::$ruedas=6; //modificamos el valor de la propiedad estatica ruedas
        echo Vehiculo::$ruedas; //mostramos el valor de la propiedad estatica ruedas
        //echo $ford->ruedas; //nos da error ya que intentamos obtener el valor de la propiedad estática ruedas como si no fuera estática
        //Vehiculo::encender(); //nos da error ya que intentamos llamar al metodo encender como si fuera estático, cuando no lo es
        ?>
    </body>
</html>
