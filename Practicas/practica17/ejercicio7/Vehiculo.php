<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vehiculo
 *
 * @author diego
 */
class Vehiculo {
    
    public $matricula;
    private $color;
    protected $encendido;
    
    function __construct()
    {
        //obtengo un array con los parámetros enviados a la función
        $params = func_get_args();
        //saco el número de parámetros que estoy recibiendo
        $num_params = func_num_args();
        //cada constructor de un número dado de parámtros tendrá un nombre de función
        //atendiendo al siguiente modelo __construct1() __construct2()...
        $funcion_constructor ='__construct'.$num_params;
        //compruebo si hay un constructor con ese número de parámetros
        if (method_exists($this,$funcion_constructor)) 
        {
            //si existía esa función, la invoco, reenviando los parámetros que recibí en el constructor original
            call_user_func_array(array($this,$funcion_constructor),$params);
        }
    }
  
	//ahora declaro una serie de métodos constructores que aceptan diversos números de parámetros
    
    function __construct1($matricula)
    {
        $this->matricula=$matricula;
    }
		
    function __construct2($matricula, $color)
    {
        $this->matricula = $matricula;
        $this->color = $color;
    }
    
    function __construct3($matricula, $color, $encendido)
    {
        $this->matricula = $matricula;
        $this->color = $color;
        $this->encendido = $encendido;
    }
    
    public function encender() 
    {
        $this->encendido=true;
        echo "vehículo encendido <br />";
    }
    
    public function apagar() 
    {
        $this->encendido=false;
        echo "vehículo apagado <br />";
    }
        
}
   
