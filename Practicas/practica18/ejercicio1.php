<?php

spl_autoload_register(function ($nombre_clase) {
    include "ejercicio1\\$nombre_clase" . '.php';
    }); 

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $camion=new Camion(); //instanciamos un nuevo objeto de la clase Camion
        $camion->encender(); //llamamos al metodo encender de la clase Camion (heredado de la clase Vehiculo) para el objeto camion
        $camion->cargar(10); //llamamos al metodo cargar de la clase Camion y le pasamos un valor requerido
        $camion->verificar_encendido(); //llamamos al metodo verificar_encendido de la clase Camion
        $camion->matricula="MDU - 293"; //damos un valor a la propiedad matricula de la clase Camion
        $camion->apagar(); //llamamos al metodo apagar de la clase Camion (heredado de la clase Vehiculo) para el objeto camion
        $autobus=new Autobus(); //instanciamos un nuevo objeto de la clase Autobus
        $autobus->encender(); //llamamos al metodo encender de la clase Autobus (heredado de la clase Vehiculo) para el objeto autobus
        $autobus->subir_pasajeros(5); //llamamos al metodo subir_pasajeros de la clase Autobus y le pasamos un valor requerido
        $autobus->verificar_encendido(); //llamamos al metodo verificar_encendido de la clase Autobus para el objeto autobus
        $autobus->matricula="KDF - 923"; //damos un valor a la propiedad matricula de la clase Camion (heredada de la clase Vehiculo)
        $autobus->apagar(); //llamamos al metodo apagar de la clase Autobus (heredado de la clase Vehiculo) para el objeto autobus
        ?>
    </body>
</html>
