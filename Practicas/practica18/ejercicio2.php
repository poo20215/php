<?php

spl_autoload_register(function ($nombre_clase) {
    include "ejercicio2\\$nombre_clase" . '.php';
    }); 

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $padre=new Persona("Ramon","Abramo",35); //creamos nuevo objeto de la clase Persona con las propiedades pasadas
        $hijo=$padre; //creamos una variable con las mismas caracteristicas que el objeto $padre (si modificamos hijo se modifica padre)
        $hija= clone $padre; //creamos una variable con las mismas caracteristicas que el objeto $padre (si modificamos hija no se modifica padre)
        $hijo->setEdad(100);//damos un nuevo valor a la propiedad edad del objeto hijo a traves de un setter
        $hija->setEdad(50);//damos un nuevo valor a la propiedad edad del objeto hija a traves de un setter
        var_dump($hijo); //obtenemos informacion del objeto hijo
        var_dump($padre); //obtenemos informacion del objeto padre
        var_dump($hija); //obtenemos informacion del objeto hija
        ?>
    </body>
</html>
