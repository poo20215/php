<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ejercicio4
 *
 * @author Alpe
 */
class Cadena 
{
    private $valor;
    private $longitud;
    private $vocales;
    
    function __construct($valor) 
    {
        $this->valor=$valor;
    }
    
    public function getValor($minusculas=FALSE) 
    {
        if(!$minusculas)
        {
            return $this->valor;
        }else 
        {
            return strtolower($this->valor);
        }
    }
    
    public function getLongitud()
    {
        $this->calcularLongitud();
        return $this->longitud;
    }
    
    public function getVocales()
    {
        $this->numeroVocales();
        return $this->vocales;
    }
    
    public function setValor($valor): void 
    {
        $this->valor = $valor;
    }

    public function setLongitud($longitud): void 
    {
        $this->longitud = $longitud;
    }

    public function setVocales($vocales): void 
    {
        $this->vocales = $vocales;
    }
    
    private function calcularLongitud()
    {
        $this->longitud=strlen($this->valor);
    }
    
    private function numeroVocales()
    {
        $vocales=["a","e","i","o","u"];
        
        foreach($vocales as $vocal)
        {
            $numero_vocales[$vocal]= substr_count($this->getValor(true),$vocal);
        }
        $this->vocales=array_sum($numero_vocales);
    }
    
    public function repeticionVocal($vocal)
    {
        $repeticion=substr_count($this->getValor(true),$vocal);
        return $repeticion;
    }

}
