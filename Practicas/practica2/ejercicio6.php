<?php

// opcion 1
// no es valido para el interior de clases
define("NOMBRE", "Diego");

//opcion 2
// es valido siempre
const NOMBRE2="Diego";

echo NOMBRE . "<br>";
echo NOMBRE2;

?>

