<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!-- Método largo -->
        Hola <?php echo $_GET["nombre"]; ?><br>
        Tu email es: <?php echo $_GET["email"]; ?>
        
        <br><br>
        
        <!-- Método recortado -->
        Hola <?= $_GET["nombre"] ?><br>
        Tu email es: <?= $_GET["email"] ?>
    </body>
</html>
