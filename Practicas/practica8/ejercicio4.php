<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio4Destino.php">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div>
            <br>
            <div>
                <label for="email">
                    Email:
                </label>
                <input type="email" name="email" id="email" placeholder="Introduce tu correo">
            </div>
            <br>
            <div>
                <button formmethod="GET">Enviar por GET</button>
                <button formmethod="POST">Enviar por POST</button>
            </div>
        </form>
    </body>
</html>
